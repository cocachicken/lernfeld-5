import {
  Button,
  Col,
  Divider,
  Form,
  Input,
  Row,
  Select,
  Slider,
  Table,
} from 'antd';
import Modal from 'antd/lib/modal/Modal';
import React, { useEffect, useState } from 'react';
import { ZutatDto } from '../../dtos/ZutatDto';
import { LieferantModel } from '../../models/LieferantModel';
import { ZutatModel } from '../../models/ZutatModel';
import lieferantService from '../../services/LieferantService';
import zutatService from '../../services/ZutatService';

const Zutaten = () => {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [allIngredients, setAllIngredients] = useState<ZutatModel[]>([]);
  const [allSuppliers, setAllSuppliers] = useState<LieferantModel[]>([]);
  const [isModalVisible, setIsModalVisible] = useState<boolean>(false);
  const [newIngredient, setnewIngredient] = useState<ZutatDto | undefined>(
    undefined
  );

  useEffect(() => {
    getAllIngredients();
    getAllSuppliers();
  }, []);

  const getAllIngredients = async (): Promise<void> => {
    setIsLoading(true);
    const response = await zutatService.getAllIngredients();
    setAllIngredients(response.data);
    setIsLoading(false);
  };

  const getAllSuppliers = async (): Promise<void> => {
    setIsLoading(true);
    const response = await lieferantService.getAllSuppliers();
    setAllSuppliers(response.data);
    setIsLoading(false);
  };

  const setTableColumns = (value: string): any => {
    return {
      title: value,
      dataIndex: value,
      key: value,
    };
  };

  const renderTableData = () => {
    return allIngredients.map((ingredient) => {
      return {
        key: ingredient.ZUTATENNR,
        IngredientId: ingredient.ZUTATENNR,
        Title: ingredient.BEZEICHNUNG,
        Unit: ingredient.EINHEIT,
        NetPrice: ingredient.NETTOPREIS,
        Duration: ingredient.BESTAND,
        Calories: ingredient.KALORIEN,
        Carbohydrates: ingredient.KOHLENHYDRATE,
        Protein: ingredient.PROTEIN,
        Supplier: ingredient.LIEFERANT.LIEFERANTENNAME,
      };
    });
  };

  const supplierOptions = (): Array<{label: string, value: number}> => {
    return allSuppliers.map((supplier) => {
      return {
        label: supplier.LIEFERANTENNAME,
        value: supplier.LIEFERANTENNR,
      };
    });
  };

  const unitOptions = (): Array<{label: string, value: string}> => {
    const existingUnits: Array<string> = [];
    allIngredients.forEach((ingredient) => {
      if (!existingUnits.includes(ingredient.EINHEIT)) {
        existingUnits.push(ingredient.EINHEIT);
      } else {
        return;
      }
    });
    return existingUnits.map((unit) => {
      return {
        label: unit,
        value: unit,
      };
    });
  };

  const createNewIngredientObject = (key: string, value: string | number): void => {
    setnewIngredient({
      ...newIngredient,
      [key]: value,
    });
  };

  const saveIngredientToDb = async (): Promise<void> => {
    if (newIngredient) {
      setIsLoading(true);
      await zutatService.createNewIngredient(newIngredient);
      setIsLoading(false);
      setIsModalVisible(false);
      setnewIngredient(undefined);
      await getAllIngredients();
    }
  };

  return (
    <div className="content">
      <Row style={{ padding: '2rem 2rem' }}>
        <Col span={24}>
          <Divider
            style={{
              color: 'white',
              borderRadius: '2px',
              fontSize: '2rem',
            }}
          >
            Ingredients
          </Divider>
          <Button
            type="link"
            style={{ marginBottom: '1rem', backgroundColor: 'white' }}
            onClick={() => setIsModalVisible(!isModalVisible)}
          >
            Create new ingredient
          </Button>
          <div className="recipetable">
            <Table
              dataSource={renderTableData()}
              columns={[
                'IngredientId',
                'Title',
                'Unit',
                'NetPrice',
                'Duration',
                'Calories',
                'Carbohydrates',
                'Protein',
                'Supplier',
              ].map((type) => setTableColumns(type))}
              loading={isLoading}
            />
          </div>
        </Col>
      </Row>
      <Modal
        destroyOnClose={true}
        title="Create new ingredient"
        visible={isModalVisible}
        onCancel={() => setIsModalVisible(!isModalVisible)}
        onOk={() => saveIngredientToDb()}
        footer={[
          <Form>
            <Form.Item>
              <Button
                key="cancel"
                onClick={() => setIsModalVisible(!isModalVisible)}
              >
                Cancel
              </Button>
              <Button
                key="submit"
                type="primary"
                loading={isLoading}
                onClick={saveIngredientToDb}
              >
                Create
              </Button>
            </Form.Item>
          </Form>,
        ]}
      >
        <Form>
          <Row>
            <Col span={24}>
              <Divider orientation="left">General information</Divider>
              <Form.Item rules={[{ required: true }]}>
                <Input
                  placeholder="Title"
                  onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                    createNewIngredientObject('Bezeichnung', e.target.value)
                  }
                ></Input>
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <Select
                placeholder="Chose a unit"
                options={unitOptions()}
                style={{ width: '100%' }}
                onChange={(e: string) =>
                  createNewIngredientObject('Einheit', e)
                }
              ></Select>
            </Col>
          </Row>
          <Divider orientation="left">Sepcial information</Divider>
          <Input
            placeholder="Net Price"
            style={{ marginBottom: '1rem' }}
            onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
              createNewIngredientObject('Nettopreis', e.target.value)
            }
            suffix="€"
          ></Input>
          <Input
            placeholder="Duration"
            onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
              createNewIngredientObject('Bestand', e.target.value)
            }
          ></Input>
          <Divider orientation="left">Nutritionals</Divider>
          <Row>
            <Col
              span={24}
              style={{ textAlign: 'center', marginBottom: '1rem' }}
            >
              <label>Calories</label>
            </Col>
            <Col span={12}>
              <Input
                placeholder="0"
                value={newIngredient?.Kalorien}
                style={{ marginBottom: '1rem' }}
                onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                  createNewIngredientObject('Kalorien', e.target.value)
                }
              ></Input>
            </Col>
            <Col span={12}>
              <Slider
                value={newIngredient?.Kalorien as number}
                onChange={(e: number) =>
                  createNewIngredientObject('Kalorien', e)
                }
                style={{ marginLeft: '1rem' }}
              />
            </Col>
          </Row>
          <Row>
            <Col
              span={24}
              style={{ textAlign: 'center', marginBottom: '1rem' }}
            >
              <label>Carbohydrates</label>
            </Col>
            <Col span={12}>
              <Input
                placeholder="0"
                value={newIngredient?.Kohlenhydrate}
                style={{ marginBottom: '1rem' }}
                onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                  createNewIngredientObject('Kohlenhydrate', e.target.value)
                }
              ></Input>
            </Col>
            <Col span={12}>
              <Slider
                value={newIngredient?.Kohlenhydrate as number}
                onChange={(e: number) =>
                  createNewIngredientObject('Kohlenhydrate', e)
                }
                style={{ marginLeft: '1rem' }}
              />
            </Col>
          </Row>
          <Row>
            <Col
              span={24}
              style={{ textAlign: 'center', marginBottom: '1rem' }}
            >
              <label>Protein</label>
            </Col>
            <Col span={12}>
              <Input
                placeholder="0"
                value={newIngredient?.Protein}
                style={{ marginBottom: '1rem' }}
                onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                  createNewIngredientObject('Protein', e.target.value)
                }
              ></Input>
            </Col>
            <Col span={12}>
              <Slider
                value={newIngredient?.Protein as number}
                onChange={(e: number) =>
                  createNewIngredientObject('Protein', e)
                }
                style={{ marginLeft: '1rem' }}
              />
            </Col>
          </Row>
          <Divider orientation="left">Supplier information</Divider>
          <Select
            options={supplierOptions()}
            style={{ width: '100%' }}
            placeholder="Chose a supplier"
            onChange={(e: string) => createNewIngredientObject('Lieferant', e)}
          ></Select>
        </Form>
      </Modal>
    </div>
  );
};

export default Zutaten;
