import { AllergieEntity } from 'src/allergie/allergie.entity';
import { ErnaehrungskategorieEntity } from 'src/ernaehrungskategorie/ernaehrungskategorie.entity';
import { ZutatEntity } from 'src/zutat/zutat.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToMany,
  JoinTable,
} from 'typeorm';

@Entity({ name: 'rezept' })
export class RecipeEntity {
  @PrimaryGeneratedColumn()
  ID: number;

  @Column()
  TITEL: string;

  @ManyToMany(type => ZutatEntity, { cascade: true, onDelete: "CASCADE" })
  @JoinTable({
    name: 'rezeptzutat',
    joinColumn: { name: 'REZEPTID', referencedColumnName: 'ID' },
    inverseJoinColumn: { name: 'ZUTATID', referencedColumnName: 'ZUTATENNR' },
  })
  ZUTATEN: ZutatEntity[];

  @ManyToMany(type => ErnaehrungskategorieEntity, { cascade: true, onDelete: "CASCADE" })
  @JoinTable({
    name: 'rezepternaehrungskategorie',
    joinColumn: { name: 'REZEPTID', referencedColumnName: 'ID' },
    inverseJoinColumn: {
      name: 'ERNAEHRUNGSKATEGORIEID',
      referencedColumnName: 'ID',
    },
  })
  KATEGORIEN: ErnaehrungskategorieEntity[];

  @ManyToMany(type => AllergieEntity, { cascade: true, onDelete: "CASCADE" })
  @JoinTable({
    name: 'rezeptallergie',
    joinColumn: { name: 'REZEPTID', referencedColumnName: 'ID' },
    inverseJoinColumn: { name: 'ALLERGIEID', referencedColumnName: 'ID' },
  })
  ALLERGIEN: AllergieEntity[];
}
