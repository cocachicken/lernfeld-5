import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { KundeController } from './kunde.controller';
import { KundeEntity } from './kunde.entity';
import { KundeRepository } from './kunde.repository';

@Module({
  imports: [TypeOrmModule.forFeature([KundeEntity, KundeRepository])],
  controllers: [KundeController],
})
export class KundeModule {}
