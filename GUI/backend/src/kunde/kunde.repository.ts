import { EntityRepository, Repository } from 'typeorm';
import { KundeEntity } from './kunde.entity';

@EntityRepository(KundeEntity)
export class KundeRepository extends Repository<KundeEntity> {
  findAllCustomers = async () => {
    return this.find({ relations: ['ALLERGIEN'] });
  };

  createNewCustomer = async (customer: KundeEntity) => {
    return this.save(customer);
  };
}
