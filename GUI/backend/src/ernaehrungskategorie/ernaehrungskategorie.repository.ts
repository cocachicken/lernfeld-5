import { EntityRepository, Repository } from 'typeorm';
import { ErnaehrungskategorieEntity } from './ernaehrungskategorie.entity';

@EntityRepository(ErnaehrungskategorieEntity)
export class ErnaehrungskategorieRepository extends Repository<ErnaehrungskategorieEntity> {
  findAllCategories = async () => {
    return this.find();
  };
}
