import { EntityRepository, Repository } from 'typeorm';
import { AllergieEntity } from './allergie.entity';

@EntityRepository(AllergieEntity)
export class AllergieRepository extends Repository<AllergieEntity> {
  findAllAllergies = async () => {
    return this.find();
  };
}
