import { WebApi } from './WebApi';
import { AxiosResponse } from 'axios';
import { BestellungModel } from '../models/BestellungModel';

export class BestellungService extends WebApi {
  async getAllRecipes(): Promise<AxiosResponse<BestellungModel[]>> {
    return this.request({
      method: 'get',
      url: '/bestellung',
    });
  }

  async setNewRecipeOrder(data: any): Promise<void> {
    this.request({
      method: 'post',
      url: '/bestellung',
      data
    })
  }
}

const bestellungService = new BestellungService();
export default bestellungService;
