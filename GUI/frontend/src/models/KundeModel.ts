import { BestellungModel } from './BestellungModel';

export interface KundeModel {
  KUNDENNR: number;
  NACHNAME: string;
  VORNAME: string;
  GEBURTSDATUM: string;
  STRASSE: string;
  HAUSNR: string;
  PLZ: string;
  ORT: string;
  TELEFON: string;
  EMAIL: string;
}
