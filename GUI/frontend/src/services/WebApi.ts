import { AxiosRequestConfig, AxiosResponse } from 'axios';

import Api from './Api';

interface Headers {
  [key: string]: string;
}

export class WebApi {
  private readonly baseUrl: string;
  private readonly withDefaultHeaders: boolean;

  constructor(
    withDefaultHeaders: boolean = true,
    baseUrl: string = 'http://localhost:3001/api' as string
  ) {
    this.baseUrl = baseUrl;
    this.withDefaultHeaders = withDefaultHeaders;
  }

  public request(requestConfig: AxiosRequestConfig): Promise<AxiosResponse> {
    return Api.request({
      ...requestConfig,
      baseURL: this.baseUrl,
      headers: this.setDefaultHeaders(requestConfig.headers),
    });
  }

  private setDefaultHeaders(headers: Headers): Headers {
    if (this.withDefaultHeaders) {
      return {
        ...headers,
        'content-type': 'application/json',
      };
    }
    return headers;
  }
}

export const webApi = new WebApi();
