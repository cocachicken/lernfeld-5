import { BestellungEntity } from 'src/bestellung/bestellung.entity';
import { LieferantEntity } from 'src/lieferant/lieferant.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToMany,
  JoinTable,
  ManyToOne,
  JoinColumn,
} from 'typeorm';

@Entity({ name: 'zutat' })
export class ZutatEntity {
  @PrimaryGeneratedColumn()
  ZUTATENNR: number;

  @Column()
  BEZEICHNUNG: string;

  @Column()
  EINHEIT: string;

  @Column()
  NETTOPREIS: number;

  @Column()
  BESTAND: number;

  @Column()
  KALORIEN: number;

  @Column()
  KOHLENHYDRATE: number;

  @Column()
  PROTEIN: number;

  @ManyToMany(type => BestellungEntity, { cascade: true, onDelete: "CASCADE" })
  @JoinTable({
    name: 'BESTELLUNGZUTAT',
    joinColumn: { name: 'ZUTATENNR', referencedColumnName: 'ZUTATENNR' },
    inverseJoinColumn: { name: 'BESTELLNR', referencedColumnName: 'BESTELLNR' },
  })
  BESTELLUNG: BestellungEntity[];

  @ManyToOne(
    type => LieferantEntity,
    lieferant => lieferant.ZUTATEN,
    { cascade: true },
  )
  @JoinColumn({
    name: 'LIEFERANT',
    referencedColumnName: 'LIEFERANTENNR',
  })
  LIEFERANT: LieferantEntity;
}
