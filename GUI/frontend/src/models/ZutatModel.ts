import { LieferantModel } from './LieferantModel';

export interface ZutatModel {
  ZUTATENNR: number;
  BEZEICHNUNG: string;
  EINHEIT: string;
  NETTOPREIS: string;
  BESTAND: string;
  KALORIEN: number;
  KOHLENHYDRATE: string;
  PROTEIN: string;
  LIEFERANT: LieferantModel;
}
