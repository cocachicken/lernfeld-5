import { Body, Controller, Get, Post } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { setNewIngredientDto } from 'src/dtos/setNewIngredientDto';
import { ZutatEntity } from './zutat.entity';
import { ZutatRepository } from './zutat.repository';

@Controller('api/zutat')
export class ZutatController {
  constructor(
    @InjectRepository(ZutatRepository)
    private readonly repository: ZutatRepository,
  ) {}

  @Get()
  getAll() {
    return this.repository.findAllIngredients();
  }

  @Post()
  setNewIngredient(@Body() ingredient: setNewIngredientDto) {
    return this.repository.setNewIngredient(ingredient);
  }
}
