import { WebApi } from './WebApi';
import { AxiosResponse } from 'axios';
import { KundeModel } from '../models/KundeModel';
import { Customer } from '../Components/Kunden/Kunden';

export class KundeService extends WebApi {
  async getAllCustomers(): Promise<AxiosResponse<KundeModel[]>> {
    return this.request({
      method: 'get',
      url: '/kunde',
    });
  }

  async createNewCustomer(
    customer: Customer
  ): Promise<AxiosResponse<Customer>> {
    return this.request({
      method: 'post',
      url: '/kunde',
      data: customer,
    });
  }
}

const kundeService = new KundeService();
export default kundeService;
