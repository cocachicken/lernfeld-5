export interface KategorieDto {
  key: number;
  value: string;
}
