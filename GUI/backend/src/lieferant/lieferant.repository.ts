import { ZutatEntity } from 'src/zutat/zutat.entity';
import { EntityRepository, getConnection, Repository } from 'typeorm';
import { LieferantEntity } from './lieferant.entity';

@EntityRepository(LieferantEntity)
export class LieferantRepository extends Repository<LieferantEntity> {
  findAllSuppliers = async () => {
    return await this.find()
  }
}
