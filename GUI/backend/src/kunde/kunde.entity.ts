import { AllergieEntity } from 'src/allergie/allergie.entity';
import { BestellungEntity } from 'src/bestellung/bestellung.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  ManyToMany,
  JoinTable,
} from 'typeorm';

@Entity({ name: 'kunde' })
export class KundeEntity {
  @PrimaryGeneratedColumn()
  KUNDENNR: number;

  @Column()
  NACHNAME: string;

  @Column()
  VORNAME: string;

  @Column()
  GEBURTSDATUM: string;

  @Column()
  STRASSE: string;

  @Column()
  HAUSNR: string;

  @Column()
  PLZ: string;

  @Column()
  ORT: string;

  @Column()
  TELEFON: string;

  @Column()
  EMAIL: string;

  @OneToMany(
    type => BestellungEntity,
    bestellung => bestellung.KUNDE,
  )
  BESTELLUNGEN: BestellungEntity[];

  @ManyToMany(type => AllergieEntity)
  @JoinTable({
    name: 'kundenallergie',
    joinColumn: { name: 'KUNDENNR', referencedColumnName: 'KUNDENNR' },
    inverseJoinColumn: { name: 'ALLERGIEID', referencedColumnName: 'ID' },
  })
  ALLERGIEN: AllergieEntity[];
}
