import React, { useEffect, useState } from 'react';
import { RezeptModel } from '../../models/RezeptModel';
import rezeptService from '../../services/RezeptService';
import { Button, Col, Divider, Input, InputNumber, Popover, Row, Select, Table, Tooltip } from 'antd';
import Modal from 'antd/lib/modal/Modal';
import zutatService from '../../services/ZutatService';
import { ZutatModel } from '../../models/ZutatModel';
import { KategorieModel } from '../../models/KategorieModel';
import kategorieService from '../../services/KategorieService';
import { ZutatDto } from '../../dtos/ZutatDto';
import { KategorieDto } from '../../dtos/KategorieDto';
import { useHistory } from "react-router-dom";
import allergieService from '../../services/AllergieService';
import { AllergieModel } from '../../models/AllergieModel';

const RezeptOverview = () => {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [allRecipes, setAllRecipes] = useState<RezeptModel[]>([]);
  const [allIngredients, setAllIngredients] = useState<ZutatModel[]>([]);
  const [allCategories, setAllCategories] = useState<KategorieModel[]>([]);
  const [allAllergies, setAllAllergies] = useState<AllergieModel[] | undefined>(undefined)
  const [isModalVisible, setIsModalVisible] = useState<boolean>(false);
  const [isPopoverVisible, setIsPopoverVisible] = useState<boolean>(false)
  const [ingredientNumber, setIngredientNumber] = useState<number>(0)
  const [newRecipeIngredients, setNewRecipeIngredients] = useState<ZutatDto[]>([]);
  const [newRecipeCategory, setNewRecipeCategory] = useState<KategorieDto | undefined>(undefined);
  const [newRecipeName, setNewRecipeName] = useState<string | undefined>(undefined);
  const [newRecipeAllergy, setNewRecipeAllergy] = useState<string | undefined>(undefined)

  useEffect(() => {
    getAllRecipes();
    getAllIngredients();
    getAllCategories();
    getAllAllergies();
  }, []);

  let history = useHistory()

  const getAllRecipes = async (): Promise<void> => {
    setIsLoading(true);
    const response = await rezeptService.getAllRecipes();
    setAllRecipes(response.data);
    setIsLoading(false);
  };

  const getAllIngredients = async (): Promise<void> => {
    const response = await zutatService.getAllIngredients();
    setAllIngredients(response.data);
  };

  const getAllCategories = async (): Promise<void> => {
    const response = await kategorieService.getAllCategories();
    setAllCategories(response.data);
  };

  const getAllAllergies = async ():Promise<void> => {
    setIsLoading(true)
    const response = await allergieService.getAllAllergies();
    setAllAllergies(response.data)
    setIsLoading(false)
  }

  const getCategoryFilters = () => {
   return allCategories.map((category) => {
      return {
        text: category.TITEL,
        value: category.TITEL
      }
    })
  }

  const getAllergyFilters = () => {
    return allAllergies?.map((allergie) => {
       return {
         text: allergie.TITEL,
         value: allergie.TITEL
       }
     })
   }

   const getIngredientFilters = () => {
    return allIngredients?.map((ingredient) => {
       return {
         text: ingredient.BEZEICHNUNG,
         value: ingredient.BEZEICHNUNG
       }
     })
   }
  
   const getRecipeFilters = () => {
    return allRecipes?.map((recipe) => {
       return {
         text: recipe.TITEL,
         value: recipe.TITEL
       }
     })
   }

  const tableColumns = [
    {
      title: 'Nr',
      dataIndex: 'ID',
      key: 'ID',
    },
    {
      title: 'Title',
      dataIndex: 'Title',
      key: 'Title',
      filters: getRecipeFilters(),
      onFilter: (value: any, record: any) => record.Title.indexOf(value) === 0
    },
    {
      title: 'Category',
      dataIndex: 'CATEGORY',
      key: 'CATEGORY',
      filters: getCategoryFilters(),
      onFilter: (value: any, record: any) => record.CATEGORY.indexOf(value) === 0
    },
    {
      title: 'Allergy',
      dataIndex: 'ALLERGY',
      key: 'ALLERGY',
      filters: getAllergyFilters(),
      onFilter: (value: any, record: any) => record.ALLERGY.indexOf(value) === 0
    },
    {
      title: 'Ingredients',
      dataIndex: 'Ingredients',
      key: 'Ingredients',
      filters: getIngredientFilters(),
      onFilter: (value: any, record: any) => record.Ingredients.indexOf(value) === 0
    },
  ];

  const renderTableData = () => {
    return allRecipes.map((recipe) => {
      return {
        key: recipe.ID,
        ID: recipe.ID,
        Title: recipe.TITEL,
        CATEGORY: recipe.KATEGORIEN.map((category) => category.TITEL).join(
          ', '
        ),
        ALLERGY: recipe.ALLERGIEN.map((allergy) => allergy.TITEL).join(', '),
        Ingredients: recipe.ZUTATEN.map((zutat) => zutat.BEZEICHNUNG).join(', '),
      };
    });
  };

  const createNewRecipe = async (): Promise<void> => {
    const mappedIngredients = newRecipeIngredients?.map((x) => {
      return {
        ZUTATENNR: x.key,
      };
    });

    const allergyId = allAllergies?.find((allergie) => allergie.TITEL === newRecipeAllergy)

    const newRecipe = [
      {
        TITEL: newRecipeName,
        KATEGORIEN: [
          {
            ID: newRecipeCategory?.key,
          },
        ],
        ZUTATEN: [...mappedIngredients],
        ALLERGIEN: [
          {
            ID: allergyId?.ID
          }
        ]
      },
    ];
    setIsLoading(true);
    await rezeptService.createNewRecipe(newRecipe);
    setNewRecipeCategory(undefined);
    setNewRecipeIngredients([]);
    setNewRecipeName(undefined);
    setIsLoading(false);
    getAllRecipes();
    setIsModalVisible(false); 
  };

  const getIngredientOptions = (): Array<{value: string, key: number}> => {
    return allIngredients.map((ingredient) => {
      return {
        value: ingredient.BEZEICHNUNG,
        key: ingredient.ZUTATENNR,
      };
    });
  };

  const getCategoryOptions = () => {
    return allCategories.map((category) => {
      return {
        value: category.TITEL,
        key: category.ID,
      };
    });
  };

  const getAllergyOptions = () => {
    return allAllergies?.map((allergy) => {
      return {
        value: allergy.TITEL,
        key: allergy.ID,
      };
    });
  };

  const handleIngredientSelectChange = (key: any, value: any) => {
    setNewRecipeIngredients(value);
  };

  const handleCategorySelectChange = (key: any, value: any) => {
    setNewRecipeCategory(value);
  };

  const getNewRecipeIngredientValues = (): (string | number)[] => {
    const ingredient = newRecipeIngredients.map((ingredient) => ingredient.value);
    return ingredient;
  };
  
  const getRecipeWithNIngredients = (amount: number) => {
   
   const recipes = allRecipes.filter((recipe) => recipe.ZUTATEN.length === amount)
  
   if(recipes.length > 0) { 
    return recipes.map((recipe) => {
    return (<li key={recipe.ID}>- {recipe.TITEL} ({recipe.ZUTATEN.length})</li>)
      })  
    } else {
      return `No Recipe with ${amount} ingredients has been found`
    }
  } 

  const onChangeIngredientNumber = (amount: number) => {
    setIngredientNumber(amount)
    setIsPopoverVisible(true)
  }
  
  const findRecipeByIngredient = () => {
    const foundRecipes = allRecipes.filter((recipe) => recipe.ZUTATEN.find((zutat) => zutat.BEZEICHNUNG === "Karotte"))
    const foundIngredients = foundRecipes.map((ingredients) => {
      return ingredients.TITEL
    })

    alert(foundIngredients.map(ingredientName => ingredientName))
  }

  const findRecipeByCategoryName = () => {
    const foundRecipes = allRecipes.filter((recipe) => recipe.KATEGORIEN.find((category) => category.TITEL === "Vegan"))
    const foundIngredients = foundRecipes.map((categories) => {
      return categories.TITEL
    })
    alert(foundIngredients.map(categoryName => categoryName))
  }

  const findRecipeWithLessThan5IngredientsAndCategoryVegatarisch = () => {
    const foundRecipe = allRecipes.filter((recipe) => recipe.ZUTATEN.length < 5 && recipe.KATEGORIEN.find((category) => category.TITEL === "Vegetarisch"))
    const foundRecipeNames = foundRecipe.map((recipe) => recipe.TITEL)
    alert(foundRecipeNames)
  }

  const findRecipeByName = () => {
    const foundrecipe = allRecipes.find((recipe) => recipe.TITEL === "Kartoffelbrot")
    console.log(foundrecipe)
  }

  return (
    <div className="content">
      <Row style={{ padding: '2rem 2rem' }}>
        <Col span={24}>
          <Divider
            style={{
              color: 'white',
              borderRadius: '2px',
              fontSize: '2rem',
            }}
          >
            Recipes
          </Divider>
          <Button
            type="link"
            style={{ marginBottom: '1rem', backgroundColor: 'white' }}
            onClick={() => setIsModalVisible(true)}
          >
            Add Recipe
          </Button>
          <Tooltip title="Search for Recipes by the amount of ingredients" placement="right">
          <Popover
            content={() => getRecipeWithNIngredients(ingredientNumber)}
            title="Recipes (Ingredients)"
            visible={isPopoverVisible}
            trigger="click"
            onVisibleChange={() => setIsPopoverVisible(!isPopoverVisible)}
        >
             <InputNumber min={0} max={100} defaultValue={0} onChange={(value: any) => onChangeIngredientNumber(value)} style={{marginLeft: "1rem", border:"0", width: "3.5rem"}} />
          </Popover>
          </Tooltip>
          <Button onClick={findRecipeByName} style={{marginLeft: "1rem"}} >Find recipe with name "Kartoffelbrot"</Button>      
          <Button onClick={findRecipeByIngredient} style={{marginLeft: "1rem"}} >Find recipe with ingredient "Karotte"</Button>
          <Button onClick={findRecipeByCategoryName} style={{marginLeft: "1rem"}} >Find recipe with category "Vegan"</Button>      
          <Button onClick={findRecipeWithLessThan5IngredientsAndCategoryVegatarisch} style={{marginLeft: "1rem", marginBottom: "1rem"}} >Find recipe with less than 5 ingredients and category "Vegetarisch"</Button>                
              <div className="recipetable">
                <Table
                  dataSource={renderTableData()}
                  columns={tableColumns}
                  loading={isLoading}
                  pagination={false}
                  onRow={(record) => {
                    return {
                      onDoubleClick: () => history.push(`/rezept/${record.ID}`)
                    };
                  }}
                />
          </div>
        </Col>
      </Row>
      <Modal
        title="Add new recipe"
        visible={isModalVisible}
        onCancel={() => setIsModalVisible(!isModalVisible)}
        onOk={() => createNewRecipe()}
        okButtonProps={{
          disabled: !newRecipeName || !newRecipeCategory,
        }}
      >
        <Row>
          <Col span={24}>
            <Divider orientation="left">General Infos</Divider>
            <Input
              placeholder="Recipename"
              onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                setNewRecipeName(e.target.value)
              }
              value={newRecipeName}
            ></Input>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <Divider orientation="left">Nutrition Category / Allergie Category</Divider>
            <Select
              placeholder="Chose a Nutritioncategory"
              options={getCategoryOptions()}
              style={{ textAlign: 'center', width: '100%' }}
              value={newRecipeCategory?.value}
              onChange={(key, value) => handleCategorySelectChange(key, value)}
            ></Select>
             <Select
              placeholder="Chose an Allergy"
              options={getAllergyOptions()}
              style={{ textAlign: 'center', width: '100%' , marginTop: "1rem"}}
              value={newRecipeAllergy}
              onChange={(e) => setNewRecipeAllergy(e)}
            ></Select>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <Divider orientation="left">Ingredient</Divider>
            <Select
              placeholder="Chose ingredients - multiple choise available"
              options={getIngredientOptions()}
              mode="tags"
              value={getNewRecipeIngredientValues()}
              onChange={(key, value) =>
                handleIngredientSelectChange(key, value)
              }
              style={{ textAlign: 'center', width: '100%' }}
            ></Select>
          </Col>
        </Row>
      </Modal>
    </div>
  );
};

export default RezeptOverview;
