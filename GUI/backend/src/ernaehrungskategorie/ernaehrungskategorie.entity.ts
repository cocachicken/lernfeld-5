import { Entity, PrimaryGeneratedColumn, Column, ManyToMany, JoinTable } from 'typeorm';

@Entity({ name: 'ERNAEHRUNGSKATEGORIE' })
export class ErnaehrungskategorieEntity {
  @PrimaryGeneratedColumn()
  ID: number;

  @Column()
  TITEL: string;  
}
