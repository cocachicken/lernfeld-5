import { Tabs } from 'antd';
import React from 'react'
import Kunden from '../Kunden/Kunden';
import Orders from '../Bestellungen/Bestellungen';
import RezeptOverview from '../Rezept/Rezept';
import Zutaten from '../Zutaten/Zutaten';

const Dashboard = () => {

  const { TabPane } = Tabs;

  return (
    <div>
        <Tabs defaultActiveKey="1" type="card" style={{padding: "1rem 1rem"}}>
          <TabPane tab="Recipes" key="1">
            <RezeptOverview />
          </TabPane>
          <TabPane tab="Orders" key="2">
          <Orders />
          </TabPane>
          <TabPane tab="Customers" key="3">
          <Kunden />
          </TabPane>
          <TabPane tab="Ingredients" key="4">
          <Zutaten />
          </TabPane>
        </Tabs>
      </div>
  )
}


export default Dashboard;