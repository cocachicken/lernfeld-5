export interface setNewIngredientDto {
  Bestand: number;
  Bezeichnung: string;
  Einheit: string;
  Kalorien: number;
  Kohlenhydrate: number;
  Lieferant: number;
  Nettopreis: number;
  Protein: number;
}
