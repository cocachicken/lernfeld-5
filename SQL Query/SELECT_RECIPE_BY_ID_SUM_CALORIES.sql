USE krautundrueben;

SELECT rezept.TITEL, SUM(zutat.KALORIEN) AS 'Gesamtkalorien'
FROM rezeptzutat
INNER JOIN zutat ON REZEPTZUTAT.ZUTATID = zutat.ZUTATENNR
INNER JOIN rezept ON REZEPTZUTAT.REZEPTID = rezept.ID
WHERE rezeptzutat.REZEPTID = 1
GROUP BY rezept.TITEL