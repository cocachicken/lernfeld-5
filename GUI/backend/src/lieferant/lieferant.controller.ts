import { Controller, Get } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { LieferantRepository } from './lieferant.repository';

@Controller('api/lieferant')
export class LieferantController {
  constructor(
    @InjectRepository(LieferantRepository)
    private readonly repository: LieferantRepository,
  ) {}

  @Get()
  findAllSuppliers() {
    return this.repository.findAllSuppliers();
  }
}
