use krautundrueben;

SELECT rezept.titel, allergie.TITEL FROM rezept
LEFT JOIN rezeptallergie
ON rezept.ID = rezeptallergie.rezeptid
left JOIN allergie
ON allergie.ID = rezeptallergie.allergieid
ORDER BY rezept.TITEL
