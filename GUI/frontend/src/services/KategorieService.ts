import { WebApi } from './WebApi';
import { AxiosResponse } from 'axios';
import { KategorieModel } from '../models/KategorieModel';

export class KategorieService extends WebApi {
  async getAllCategories(): Promise<AxiosResponse<KategorieModel[]>> {
    return this.request({
      method: 'get',
      url: '/ernaehrungskategorie',
    });
  }
}

const kategorieService = new KategorieService();
export default kategorieService;
