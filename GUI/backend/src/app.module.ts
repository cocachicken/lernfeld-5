import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm/dist/typeorm.module';
import { RecipeEntity } from './rezept/recipes.entity';
import { RecipesModule } from './rezept/recipes.module';
import { ZutatEntity } from './zutat/zutat.entity';
import { ZutatModule } from './zutat/zutat.module';
import { ErnaehrungskategorieModule } from './ernaehrungskategorie/ernaehrungskategorie.module';
import { ErnaehrungskategorieEntity } from './ernaehrungskategorie/ernaehrungskategorie.entity';
import { KundeModule } from './kunde/kunde.module';
import { KundeEntity } from './kunde/kunde.entity';
import { BestellungModule } from './bestellung/bestellung.module';
import { BestellungEntity } from './bestellung/bestellung.entity';
import { LieferantModule } from './lieferant/lieferant.module';
import { LieferantEntity } from './lieferant/lieferant.entity';
import { AllergieModule } from './allergie/allergie.module';
import { AllergieEntity } from './allergie/allergie.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: '',
      database: 'krautundrueben',
      entities: [
        RecipeEntity,
        ZutatEntity,
        ErnaehrungskategorieEntity,
        KundeEntity,
        BestellungEntity,
        LieferantEntity,
        AllergieEntity,
      ],
      synchronize: false,
    }),
    RecipesModule,
    ZutatModule,
    ErnaehrungskategorieModule,
    KundeModule,
    BestellungModule,
    LieferantModule,
    AllergieModule,
  ],
})
export class AppModule {}
