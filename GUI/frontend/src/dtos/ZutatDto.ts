export interface ZutatDto {
  [key: string]: string | number;
}
