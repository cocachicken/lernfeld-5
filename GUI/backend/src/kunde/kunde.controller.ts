import { Body, Controller, Get, Post } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { KundeEntity } from './kunde.entity';
import { KundeRepository } from './kunde.repository';

@Controller('api/kunde')
export class KundeController {
  constructor(
    @InjectRepository(KundeRepository)
    private readonly repository: KundeRepository,
  ) {}

  @Get()
  getAllCostumers() {
    return this.repository.findAllCustomers();
  }

  @Post()
  createNewCustomer(@Body() customer: KundeEntity) {
    return this.repository.createNewCustomer(customer);
  }
}
