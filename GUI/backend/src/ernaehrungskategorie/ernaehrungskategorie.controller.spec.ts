import { Test, TestingModule } from '@nestjs/testing';
import { ErnaehrungskategorieController } from './ernaehrungskategorie.controller';

describe('ErnaehrungskategorieController', () => {
  let controller: ErnaehrungskategorieController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ErnaehrungskategorieController],
    }).compile();

    controller = module.get<ErnaehrungskategorieController>(ErnaehrungskategorieController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
