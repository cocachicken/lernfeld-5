import { ZutatEntity } from 'src/zutat/zutat.entity';
import {
  Column,
  Entity,
  JoinColumn,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity({ name: 'lieferant' })
export class LieferantEntity {
  @PrimaryGeneratedColumn()
  LIEFERANTENNR: number;

  @Column()
  LIEFERANTENNAME: string;

  @Column()
  STRASSE: string;

  @Column()
  HAUSNR: string;

  @Column()
  PLZ: string;

  @Column()
  ORT: string;

  @Column()
  TELEFON: string;

  @Column()
  EMAIL: string;

  @OneToMany(
    type => ZutatEntity,
    zutat => zutat.LIEFERANT,
  )
  ZUTATEN: ZutatEntity[];
}
