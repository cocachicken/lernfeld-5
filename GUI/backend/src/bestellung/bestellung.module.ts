import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BestellungController } from './bestellung.controller';
import { BestellungEntity } from './bestellung.entity';
import { BestellungRepository } from './bestellung.repository';

@Module({
  imports: [TypeOrmModule.forFeature([BestellungEntity, BestellungRepository])],
  controllers: [BestellungController],
})
export class BestellungModule {}
