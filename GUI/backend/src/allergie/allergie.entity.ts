import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToMany,
  JoinTable,
} from 'typeorm';

@Entity({ name: 'allergie' })
export class AllergieEntity {
  @PrimaryGeneratedColumn()
  ID: number;

  @Column()
  TITEL: string;
}
