USE krautundrueben;

SELECT * FROM rezeptzutat
LEFT JOIN rezepternaehrungskategorie ON rezeptzutat.REZEPTID = rezepternaehrungskategorie.REZEPTID
GROUP BY rezeptzutat.REZEPTID, rezepternaehrungskategorie.REZEPTID
HAVING COUNT(rezeptzutat.ZUTATID) < 5 AND rezepternaehrungskategorie.ERNAEHRUNGSKATEGORIEID = 2

