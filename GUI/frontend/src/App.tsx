import React from 'react';
import './App.css';
import 'antd/dist/antd.css';
import { Route, BrowserRouter as Router } from 'react-router-dom';
import RezeptDetail from './Components/RezeptDetail/RezeptDetail';
import Dashboard from './Components/Dashboard/Dashboard';
import Header from './Components/Header/Header';

function App() {
  return (
    <div className="App">
            <Header />
      <Router>
        <Route exact path="/" component={Dashboard} />
        <Route path="/rezept/:id" component={RezeptDetail}/>
      </Router>
    </div>
  );
}

export default App;
